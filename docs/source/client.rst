.. _client:

Client
======

We understant a client as the person who requires a service of any kind.

**Client Object**

+-------------------+---------------------------+
| Attribute         |        Description        |
+===================+===========================+
| client_id         | **String**                |
+-------------------+---------------------------+
| user_id           | **String**                |
+-------------------+---------------------------+
| first_name        | **String**                |
+-------------------+---------------------------+
| last_name         | **String**                |
+-------------------+---------------------------+
| cell_phone        | **String**                |
+-------------------+---------------------------+
| favorite_category | **String**                |
+-------------------+---------------------------+
| email             | **String**                |
+-------------------+---------------------------+

New Client
^^^^^^^^^^

Creates a new client

**Definition**

POST ``https://semple.app/api/v1/user/client``

::

    {
        "first_name" : "Perry",
        "last_name" : "Owens",
        "photo": "https://randomuser.me/api/portraits/men/28.jpg",
        "email" : "perry.owens@example.com",
        "phone" : "(292)-554-2654",
        "password" : "123"
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "token": "YToyOntzOjc6InVzZXJfaWQiO3M6MToiOSI7czo5OiJjbGllbnRfaWQiO3M6MToiNyI7fQ==",
        "message": "User added"
    }

List Clients
^^^^^^^^^^^^

List all clients.

**Definition**

GET ``https://semple.app/api/v1/user/client``

**Response**

::

    {
        "status": "200",
        "data": [
            {
                "user_id": "1",
                "type": "client",
                "photo": "http://randomweb.com/img/default-picture.jpg",
                "first_name": "Rodrigo José",
                "last_name": "Sánchez Segovia",
                "email": "rodrigo@locker.com.mx",
                "phone": "9991321564",
                "cell_phone": null,
                "password": "202cb962ac59075b964b07152d234b70",
                "distance_range": "0",
                "creation_date": "2018-11-13 13:06:11",
                "last_login": "2018-11-26 19:10:28",
                "calificacion": "0",
                "favorite_category": "1,2,5,7",
                "status": "visible",
                "user_order": "1"
            },
            {
                "user_id": "4",
                "type": "client",
                "photo": "http://randomweb.com/img/default-picture.jpg",
                "first_name": "John A.",
                "last_name": "Smith",
                "email": "john@email.com",
                "phone": "99912345678",
                "cell_phone": null,
                "password": "202cb962ac59075b964b07152d234b70",
                "distance_range": "0",
                "creation_date": "2018-11-26 17:20:19",
                "last_login": "2018-11-27 12:30:41",
                "calificacion": "0",
                "favorite_category": null,
                "status": "hidden",
                "user_order": "4"
            },
            {
                "user_id": "5",
                "type": "client",
                "photo": "http://randomweb.com/img/default-picture.jpg",
                "first_name": "Roscoe M.",
                "last_name": "Johns",
                "email": "roscoe@email.com",
                "phone": "99912315678",
                "cell_phone": null,
                "password": "202cb962ac59075b964b07152d234b70",
                "distance_range": "0",
                "creation_date": "2018-11-27 12:23:24",
                "last_login": null,
                "calificacion": "0",
                "favorite_category": null,
                "status": "visible",
                "user_order": "5"
            }
        ]
    }

Get existing client
^^^^^^^^^^^^^^^^^^^

Get the actual information of an existing client. It's needed to specify the user's ``token``.

**Request**

+-------------+-----------------------+
|  Attribute  |  Description          |
+-------------+-----------------------+
| token       | **string** (Required) |
+-------------+-----------------------+

**Response**

::

    {
        "status": "200",
        "data": {
            "user_id": "5",
            "type": "client",
            "photo": "http://randomweb.com/img/default-picture.jpg",
            "first_name": "Roscoe",
            "last_name": "Johns",
            "email": "roscoe@email.com",
            "phone": "99912315678",
            "cell_phone": null,
            "password": "202cb962ac59075b964b07152d234b70",
            "distance_range": "0",
            "creation_date": "2018-11-27 12:23:24",
            "last_login": null,
            "calificacion": "0",
            "favorite_category": null,
            "status": "visible",
            "user_order": "5"
        }
    }

Favorite Categories
^^^^^^^^^^^^^^^^^^^

All client's has the oportunity to select favorite categories. The limit number for selecting is **6**.
In order to save the favorite categories, we must send an ``array`` of id's, each ``id`` belongs to a category.

**Definition**

POST ``https://semple.app/api/v1/user/client/{token}/category``

::
    
    {
        "category" : [
            "1",
            "2",
            "4"
        ]
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "message": "All rows inserted"
    }

Login Client
^^^^^^^^^^^^

Data required to login

**Definition**

POST ``https://semple.app/api/v1/user/client/login``

::

    {
        "email" : "john@client.com",
        "password" : "clientpassword"
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "token": "YToyOntzOjc6InVzZXJfaWQiO3M6MToiMiI7czo1OiJ0b2tlbiI7czoxOiIxIjt9",
        "message": "login successfull"
    }