User
====

The users are resources in semple that manage accounts for ``clients`` and ``providers``.

User Object
^^^^^^^^^^^

+--------------+-----------------------------------------------------+
| Attribute    | Definition                                          |
+--------------+-----------------------------------------------------+
| user_id      | **integer** Unique id                               |
+--------------+-----------------------------------------------------+
| type         | **enum** Defines the account: [client_, provider]   |
+--------------+-----------------------------------------------------+
| device_token | **string** Firebase token for push notifications    |
+--------------+-----------------------------------------------------+
| photo        | **string** Photo for user                           |
+--------------+-----------------------------------------------------+
| email        | **string** Email for login                          |
+--------------+-----------------------------------------------------+
| password     | **string** User's password                          |
+--------------+-----------------------------------------------------+

Update User
^^^^^^^^^^^

Updates an existing user (Client or Provider).
``token`` is obtained after login

**Request** 

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+

**Definition**

PUT ``https://semple.app/api/v1/user/{token}``

::

    {
        "first_name" : "Perry",
        "last_name" : "Owens",
        "email" : "rodrigo@cliente.com",
        "phone" : "9479096140"
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "message": "User updated"
    }

Update User's Password
^^^^^^^^^^^^^^^^^^^^^^

Updates an existing user's password (Client or Provider)
``token`` is obtained after login

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+

**Definition**

PUT ``https://semple.app/api/v1/user/{token}/password``

::

    {
        "password" : "my_current_password",
        "new_password" : "my_new_password"
    }


User Settings
^^^^^^^^^^^^^

All users has several options to improve the experience while using the application.

+---------------------+-----------------------------------------------------------------------------------+
| Attribute           | Definition                                                                        |
+---------------------+-----------------------------------------------------------------------------------+
| chat                | **boolean** Toggle chat notifications                                             |
+---------------------+-----------------------------------------------------------------------------------+
| caducidad           | **boolean** Toggle notification when service request is about to expire           |
+---------------------+-----------------------------------------------------------------------------------+
| work_available      | **boolean** Toggle notification when provider sent proposal (Client)              |
+---------------------+-----------------------------------------------------------------------------------+
| service_request     | **boolean** Toggle notification when service request is available (Providers)     |
+---------------------+-----------------------------------------------------------------------------------+
| accept_service      | **boolean** Toggle notification when client accepts provider (Providers)          |
+---------------------+-----------------------------------------------------------------------------------+
| max_range           | **integer** Max range for search or be searched                                   |
+---------------------+-----------------------------------------------------------------------------------+

Get User Settings
^^^^^^^^^^^^^^^^^

Settings for the application

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+

**Definition**

GET ``https://semple.app/api/v1/user/{token}/settings``

**Response**

::

    {
        "status": 200,
        "success": true,
        "data": {
            "user_settings_id": "1",
            "user_id": "1",
            "chat": true,
            "caducidad": true,
            "work_available": true,
            "service_request": true,
            "rate": false,
            "max_range": "10"
        }
    }

Update User Settings
^^^^^^^^^^^^^^^^^^^^

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+

**Definition**

PUT ``https://semple.app/api/v1/user/{token}/settings``

::

    {
        "chat" : "1",
        "caducidad" : "0",
        "work_available" : "1",
        "service_request" : "1",
        "accept_service" : "0",
        "max_range" : "5"
    }

**Response**

::

    {
        "status": 200,
        "success": true
    }
