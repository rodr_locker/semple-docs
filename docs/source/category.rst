Category
========

Categories are resources in semple that allows to standardize the offers that the market may demand. All categories has subcategories.

Category Object
^^^^^^^^^^^^^^^

+---------------+------------------------------------------------------------------+
| **Attribute** | **Definition**                                                   |
+---------------+------------------------------------------------------------------+
| category_id   | **integer** Unique Id for the category                           |
+---------------+------------------------------------------------------------------+
| name          | **string** Name of the category                                  |
+---------------+------------------------------------------------------------------+
| icon          | **string** Url for the icon image                                |
+---------------+------------------------------------------------------------------+
| photo         | **string** Url for the cover image                               |
+---------------+------------------------------------------------------------------+
| keyword       | **string** List of accepted keyword                              |
+---------------+------------------------------------------------------------------+
| status        | **string** Toggle visibility (``visible``, ``hidden``)           |
+---------------+------------------------------------------------------------------+

Get All Categories
^^^^^^^^^^^^^^^^^^

List all categories available.

**Definition**

GET ``https://semple.app/api/v1/category``

**Response**

::

    {
        "status": "200",
        "data": [
            {
                "category_id": "1",
                "name": "Eventos y fiestas",
                "icon": "http://randomweb.com/resources/d1e0435fb79de2c8.png",
                "photo": "http://randomweb.com/resources/d06c7df8d456aed0.jpg",
                "keyword": "eventos y fiestas, eventos, fiestas",
                "status": "visible",
                "category_order": "1"
            },
            {
                "category_id": "2",
                "name": "Construcción y remodelación",
                "icon": "http://randomweb.com/resources/f9fe735914210c98.png",
                "photo": "http://randomweb.com/resources/322d991e7fba927f.jpg",
                "keyword": "construccion y remodelacion, construccion, remodelacion",
                "status": "visible",
                "category_order": "2"
            },
            {
                "category_id": "3",
                "name": "Mantenimiento General",
                "icon": "http://randomweb.com/resources/343db8548a9b52e7.png",
                "photo": "http://randomweb.com/resources/d7b1368aa5cc4762.jpg",
                "keyword": "mantenimiento general, mantenimiento, general",
                "status": "visible",
                "category_order": "3"
            },
            {
                "category_id": "4",
                "name": "Mantenimiento electrónica",
                "icon": "http://randomweb.com/resources/9df794de5ec46350.png",
                "photo": "http://randomweb.com/resources/d9de7e813687eaca.jpg",
                "keyword": "mantenimiento electronica, mantenimiento, electronica",
                "status": "visible",
                "category_order": "4"
            },
            {
                "category_id": "5",
                "name": "Servicios Profesionales",
                "icon": "http://randomweb.com/resources/b1651c002b6f2cb6.png",
                "photo": "http://randomweb.com/resources/6bc28781ccd17004.jpg",
                "keyword": "servicios profecionales, servicios, profecionales",
                "status": "visible",
                "category_order": "5"
            },
            {
                "category_id": "6",
                "name": "Cuidado de personas",
                "icon": "http://randomweb.com/resources/ad2f10a4464d81df.png",
                "photo": "http://randomweb.com/resources/ac318582c52950ea.jpg",
                "keyword": "cuidado de personas, cuidado",
                "status": "visible",
                "category_order": "6"
            },
            {
                "category_id": "7",
                "name": "Servicios de limpieza",
                "icon": "http://randomweb.com/resources/984c52e21d6f8e13.png",
                "photo": "http://randomweb.com/resources/67ea7473a571a156.jpg",
                "keyword": "servicios de limpieza, servicios, limpieza",
                "status": "visible",
                "category_order": "7"
            },
            {
                "category_id": "8",
                "name": "Marketing / Publicidad",
                "icon": "http://randomweb.com/resources/5c0630419962799b.png",
                "photo": "http://randomweb.com/resources/122d61c8d7537966.jpg",
                "keyword": "marketing, publicidad",
                "status": "visible",
                "category_order": "8"
            },
            {
                "category_id": "11",
                "name": "Asesorías Escolares",
                "icon": "http://randomweb.com/resources/f729f1c0619dac35.png",
                "photo": "http://randomweb.com/resources/84a9743fbfc1db89.jpg",
                "keyword": "asesorias escolares, asesorias, escolares",
                "status": "visible",
                "category_order": "11"
            },
            {
                "category_id": "12",
                "name": "Animales y mascotas",
                "icon": "http://randomweb.com/resources/4ff23b9fbee99b79.png",
                "photo": "http://randomweb.com/resources/5d0c2781476a2f58.jpg",
                "keyword": "animales y mascotas, animales, mascotas",
                "status": "visible",
                "category_order": "12"
            },
            {
                "category_id": "13",
                "name": "Servicios para ropa",
                "icon": "http://randomweb.com/resources/cc4ec21527376c61.png",
                "photo": "http://randomweb.com/resources/0656f01efe2cac12.jpg",
                "keyword": "servicios para ropa, servicios, ropa",
                "status": "visible",
                "category_order": "13"
            },
            {
                "category_id": "14",
                "name": "Seguridad y limpieza de negocios",
                "icon": "http://randomweb.com/resources/df16d3ddeb06f2b5.png",
                "photo": "http://randomweb.com/resources/1f59fa0266b2f256.jpg",
                "keyword": "seguridad, limpieza, negocios",
                "status": "visible",
                "category_order": "14"
            },
            {
                "category_id": "15",
                "name": "Fletes",
                "icon": "http://randomweb.com/resources/723c1150c0ca8fdc.png",
                "photo": "http://randomweb.com/resources/ce5e73ca65e2e365.jpg",
                "keyword": "fletes",
                "status": "visible",
                "category_order": "15"
            },
            {
                "category_id": "16",
                "name": "Mecánica automotriz",
                "icon": "http://randomweb.com/resources/53e0ed7b07e01eab.png",
                "photo": "http://randomweb.com/resources/a25f0c9c7158e40a.jpg",
                "keyword": "mecanica, automotriz",
                "status": "visible",
                "category_order": "16"
            },
            {
                "category_id": "17",
                "name": "Servicios varios",
                "icon": "http://randomweb.com/resources/8e59cd60bc51513d.png",
                "photo": "http://randomweb.com/resources/75255a39fbf5bfb4.jpg",
                "keyword": "servicios, varios",
                "status": "visible",
                "category_order": "17"
            }
        ]
    }


