Authentication
===============

Authentication keys are used to authenticate all of your api calls via HTTP Basic Auth. Be careful about sharing private keys as they have very powerful privileges.

All requests most include an authentication token at the header:

::

    Authentication: Basic QWViQm5XTUNHbTFGb0c3MVJoUjVJbFZKR3Z3QUtSc2lrd1J1U0pubS10dHp5OG5WMlhUS1AzWTRoUTc3U25vNkpEZjA5M0xiTTdycVNkaFo6RU8zN2o2dWJnaUxfRGNEbTJ4YjVQVjZSemttdURQVXJEVVJaQTlIdlo5RmVTT2JvQzMwR0xNLURTZHV5QmdPcDFXZ1EyWTBheVpBZkdFTmE=

