Service
=======

Definition for services, all services needs to include a Location, the requested time, a Picture, Subcategory and a short description.

Create Service Request
^^^^^^^^^^^^^^^^^^^^^^

Creates a new service request by Client.

**Request**

+-------------+-----------------------+
|  Attribute  |  Description          |
+-------------+-----------------------+
| token       | **string** (Required) |
+-------------+-----------------------+

**Definition**

POST ``https://semple.app/api/v1/user/client/{token}/service/request``

::

    {
        "location_id" : "1",
        "subcategory_id" : "7",
        "request_service_time" : "2020-01-25 17:32:20",
        "photo" : "https://bwm-test-1.s3.amazonaws.com/1234567890123",
        "description" : "I need a walk for my dog"
    }

**Response**

::

    {
        "status": 200,
        "success": true,
        "message": "Request created"
    }

List Service Request
^^^^^^^^^^^^^^^^^^^^

List all active services by the client. This services has a live period of 45 mins.

**Request**

+-------------+-----------------------+
|  Attribute  |  Description          |
+-------------+-----------------------+
| token       | **string** (Required) |
+-------------+-----------------------+

**Definition**

GET ``https://semple.app/api/v1/user/client/{token}/service/request``

**Response**

::

    {
        "status": "200",
        "success": true,
        "data": [
            {
                "service_request_id": "145",
                "user_id": "6",
                "location_id": "1",
                "subcategory_id": "7",
                "requested_service_time": "2020-09-22 13:32:20",
                "request_expiration_time": "2020-09-22 14:21:50",
                "photo": "https://bwm-test-1.s3.amazonaws.com/1234567890123",
                "description": "I need a walk for my dog",
                "time": "13:32:20",
                "date": "2020-09-22",
                "status": "visible",
                "category_name": "Mantenimiento General",
                "subcategory_name": "Plomero",
                "icon": "https://example.domain/img/categoria/343db8548a9b52e7.png",
                "responses": "1"
            }
        ]
    }

Send Proposal As Provider
^^^^^^^^^^^^^^^^^^^^^^^^^

Send Proposal as Provider

**Request**

+-------------+-----------------------+
|  Attribute  |  Description          |
+-------------+-----------------------+
| token       | **string** (Required) |
+-------------+-----------------------+

**Definition**

POST ``https://semple.app/api/v1/user/provider/{token}/service/proposal``

::

    {
        "service_request_id": "145",
        "proposed_service_time": "2020-09-22 16:34:00",
        "service_price": "199",
        "service_location": "providers_place"
    }

**Response**

::

    {
        "success": true,
        "case": 1,
        "message": "1 row(s) affected."
    }


List Applied Providers To Service Request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Request**

+--------------------------+-----------------------+
|  Attribute               |  Description          |
+--------------------------+-----------------------+
| token                    | **string** (Required) |
+--------------------------+-----------------------+
| service_request_id       | **int** (Required)    |
+--------------------------+-----------------------+

**Definition**

GET ``https://semple.app/api/v1/user/client/{token}/service/request/{service_request_id}``

**Response**

::

    {
        "status": "200",
        "success": true,
        "data": [
            {
                "service_proposal_id": "139",
                "service_request_id": "145",
                "provider_id": "2",
                "proposed_service_time": "2020-09-22 16:34:00",
                "service_price": "199",
                "service_location": "Servicio en empresa",
                "distance": "10.31",
                "status": "created",
                "creation_date": "2020-09-22 11:38:38",
                "photo": "",
                "email": "rodrigo@proveedor.com",
                "name": "Rodrigo Sanchez Segovia",
                "cell_phone": "+529995763360",
                "phone": "98440041",
                "rate": "83.08",
                "stars": 4.15
            }
        ]
    }

Contact Provider
^^^^^^^^^^^^^^^^

Enables the communication between client and provider, also updates the status for both Service Request and Service Proposal to ``hired``.

**Definition**

PUT ``https://semple.app/api/v1/service/hire``

::

    {
	    "service_proposal_id" : "139",
        "token" : "YToyOntzOjc6InVzZXJfaWQiO3M6MToiNiI7czo5OiJjbGllbnRfaWQiO3M6MToiMyI7fQ=="
    }

**Response**

::

    {
        "status": 200,
        "success": true,
        "message": "1 row(s) affected"
    }

Confirm Service Request As Client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Move the service to the history section for the clients.

**Definition**

PUT ``https://semple.app/api/v1/service/accept``

::

    {
	    "service_request_id" : "145",
        "token" : "YToyOntzOjc6InVzZXJfaWQiO3M6MToiNiI7czo5OiJjbGllbnRfaWQiO3M6MToiMyI7fQ=="
    }


In Process Services
^^^^^^^^^^^^^^^^^^^

List all in process services at history section

+--------------------------+-----------------------+
|  Attribute               |  Description          |
+--------------------------+-----------------------+
| token                    | **string** (Required) |
+--------------------------+-----------------------+

**Definition**

GET ``https://semple.app/api/v1/user/client/{token}/accepted``

::

    {
        "status": 200,
        "success": true,
        "data": [
            {
                "service_proposal_id": "130",
                "service_request_id": "124",
                "provider_id": "2",
                "proposed_service_time": "2020-02-10 00:21:00",
                "service_price": "999.99",
                "service_location": "Servicio en empresa",
                "distance": "0.77",
                "status": "confirmed",
                "creation_date": "2020-02-09 22:21:18",
                "proposal_status": "end",
                "description": "fg",
                "name": "John Smith",
                "cell_phone": "+521194763360",
                "phone": "98440041",
                "email": "john@proveedor.com",
                "photo": "",
                "category_icon": "http://demosite.com/img/categoria/8e59cd60bc51513d.png",
                "category_name": "Servicios varios",
                "subcategory_name": "Lavado auto a domicilio",
                "location_desc": "Calle 29 441, Amapola, 97219 Mérida, Yuc., Mexico",
                "street": null,
                "number": null,
                "neighborhood": null,
                "score": "60",
                "favorite": true,
                "status_desc": "En proceso",
                "folio": "SELA124"
            },
            {
                "service_proposal_id": "115",
                "service_request_id": "108",
                "provider_id": "2",
                "proposed_service_time": "2020-01-13 16:25:25",
                "service_price": "199",
                "service_location": "Servicio en empresa",
                "distance": "0",
                "status": "confirmed",
                "creation_date": "2020-01-13 14:25:45",
                "proposal_status": "confirmed",
                "description": "ff",
                "name": "John Doe",
                "cell_phone": "+52992332763360",
                "phone": "98440041",
                "email": "doe@proveedor.com",
                "photo": "",
                "category_icon": "http://demosite.com/img/categoria/8e59cd60bc51513d.png",
                "category_name": "Servicios varios",
                "subcategory_name": "Lavado auto a domicilio",
                "location_desc": "Calle 41 527 ",
                "street": "Calle 41",
                "number": "527",
                "neighborhood": "",
                "score": null,
                "favorite": true,
                "status_desc": "En proceso",
                "folio": "SELA108"
            },
        ]
    }


Detail Process Services
^^^^^^^^^^^^^^^^^^^^^^^

+--------------------------+-----------------------+
|  Attribute               |  Description          |
+--------------------------+-----------------------+
| token                    | **string** (Required) |
+--------------------------+-----------------------+
| service_proposal_id      | **string** (Required) |
+--------------------------+-----------------------+

**Definition**

GET ``https://semple.app/api/v1/user/client/{token}/service_detail/{service_proposal_id}``

::

    {
        "success": true,
        "data": {
            "service_proposal_id": "152",
            "service_request_id": "183",
            "provider_id": "2",
            "proposed_service_time": "2020-09-30 18:06:21",
            "service_price": "555.55",
            "service_location": "providers_place",
            "distance": "0.14",
            "status": "hired",
            "creation_date": "2020-09-30 16:06:55",
            "description": "hi",
            "category_icon": "http://demosite.com/img/categoria/f9fe735914210c98.png",
            "category_name": "Construcción y remodelación",
            "subcategory_name": "Aluminiero",
            "service_name": "Aluminiero",
            "location_desc": "Calle 36 296 Calle 36 296, Miguel Alemán, 97148 Mérida, Yuc., México",
            "street": "Calle 36",
            "number": "296",
            "internal_number": "",
            "between_streets": "",
            "neighborhood": "Calle 36 296, Miguel Alemán, 97148 Mérida, Yuc., México",
            "photo": "",
            "name": "John Smith",
            "cell_phone": "+529997766460",
            "phone": "98440041",
            "email": "john@proveedor.com",
            "score": 0,
            "folio": "COAL183"
        }
    }