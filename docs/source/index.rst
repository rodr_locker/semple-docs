.. Semple documentation master file, created by
   sphinx-quickstart on Thu Sep  3 11:26:31 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Overview
==================================

The Semple's API is designed on REST, therefore you will find that the URLs are resource oriented and HTTP response codes are used to indicate errors in the API.
All API responses are in JSON format, including errors.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   authentication
   endpoint
   user
   client
   address
   category
   service
