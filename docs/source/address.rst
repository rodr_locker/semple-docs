Addresses
=========

Inside the application the clients are able to save their addresses, delete them, get one in specific and list them. There can be multiple addreses for one client.

Address Object
^^^^^^^^^^^^^^

+---------------------+------------------------------------------------+
| Attribute           | Definition                                     |
+---------------------+------------------------------------------------+
| address_id          | **Int** unique id for this record              | 
+---------------------+------------------------------------------------+
| user_id             | **Int** reference in this section              |
+---------------------+------------------------------------------------+
| location_id         | **Int** reference in this section (location_.) |
+---------------------+------------------------------------------------+
| name                | **String**                                     |
|                     | Name for this record                           |
+---------------------+------------------------------------------------+
| street              | **String**                                     |
+---------------------+------------------------------------------------+
| number              | **String**                                     |
+---------------------+------------------------------------------------+
| internal_number     | **String**                                     |
+---------------------+------------------------------------------------+
| between_streets     | **String**                                     |
+---------------------+------------------------------------------------+
| neighborhood        | **String**                                     |
+---------------------+------------------------------------------------+
| state               | **String**                                     |
+---------------------+------------------------------------------------+
| city                | **String**                                     |
+---------------------+------------------------------------------------+
| zip_code            | **String**                                     |
+---------------------+------------------------------------------------+

List Addresses
^^^^^^^^^^^^^^

List all addresses for a client

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+

GET ``https://semple.app/api/v1/user/client/{token}/address``

**Response**

::

    {
        "status": "200",
        "success": true,
        "data": [
            {
                "address_id": "2",
                "user_id": "8",
                "location_id": "5",
                "name": "Homa",
                "street": "36",
                "number": "450",
                "internal_number": "55",
                "between_streets": "23 x 54",
                "neighborhood": "Great Avenue",
                "state": "State",
                "city": "City",
                "zip_code": "97000"
            }
        ]
    }

Add address
^^^^^^^^^^^

Creates an address for a client

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string** (Required) |
+---------------------+-----------------------+


**Definition**

POST ``https://semple.app/api/v1/user/client/{token}/address``

::

    {
        "location" : {
            "latitude": "21.009950",
            "longitude":"-89.584320"
        },
        "name" : "Recurrente",
        "street" : "36",
        "number" : "450",
        "internal_number" : "",
        "between_streets" : "22 x 24",
        "neighborhood" : "Vista Alegre",
        "state" : "Yucatán",
        "city" : "Mérida",
        "zip_code" : "97113"
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "message": "Adress created"
    }

Update Address
^^^^^^^^^^^^^^

Update an existing address for a client

**Request**

+---------------------+-----------------------------------+
| Attribute           | Definition                        |
+---------------------+-----------------------------------+
| token               | **string** (Required)             |
+---------------------+-----------------------------------+
| address_id          | **integer** Unique id for address |
+---------------------+-----------------------------------+

**Definition**

PUT ``https://semple.app/api/v1/user/client/{token}/address/{address_id}``

::

    {
        "location" : {
            "location_id" : "1",
            "latitude" : "21.0099500",
            "longitude" : "-89.5843200"
        },
        "name" : "Home",
        "street" : "36",
        "number" : "450",
        "internal_number" : "",
        "between_streets" : "22 x 24",
        "neighborhood" : "Montebello",
        "state" : "Yucatán",
        "city" : "Mérida",
        "zip_code" : "97113"
    }

Delete Address
^^^^^^^^^^^^^^

Deletes an existing address for a client

**Request**

+---------------------+-----------------------------------+
| Attribute           | Definition                        |
+---------------------+-----------------------------------+
| address_id          | **integer** Unique id for address |
+---------------------+-----------------------------------+

**Definition**

DELETE ``https://semple.app/api/v1/user/client/address/{address_id}``

**Response**

::

    {
        "status": "200",
        "success": true,
        "message": "Address deleted"
    }

.. _location:

Location
========

Locations contains the coordinates for any address. Location can be created without addresses, but addresses can not be created without locations

Location Object
^^^^^^^^^^^^^^^

+------------+-------------------------+
| Attribute  | Definition              |
+------------+-------------------------+
| id         | **integer**             |
|            | Unique ID for location  |
+------------+-------------------------+
| latitude   | **float**               |
|            | Latitude value          |
+------------+-------------------------+
| longitude  | **float**               |
|            | Longitude value         |
+------------+-------------------------+

Add Location
^^^^^^^^^^^^

Creates a new location.

**Request**

+---------------------+-----------------------+
| Attribute           | Definition            |
+---------------------+-----------------------+
| token               | **string**            |
|                     |                       |
|                     | (Required)            |
+---------------------+-----------------------+

**Definition**

POST ``https://semple.app/api/v1/user/{token}/location``

::

    {
        "latitude": "22.009950",
        "longitude": "-87.584320",
        "description": "Street grove green"
    }

**Response**

::

    {
        "status": "200",
        "success": true,
        "message": "1 row(s) affected"
    }
