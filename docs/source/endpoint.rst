API Endpoints
=============


The Semple's REST API has a sandbox and a production environment. Use the credentials that were generated at the time of the registration to integrate the application with semple. Once you are ready to go to production and your application is approved, update credentials to access the production environment.

The following URIs are the basis of endpoints for supported environments:

- **Tests**, Base URI:

``https://sandbox.semple.app/api``

- **Production**, Base URI:

``https://semple.app/api``

A complete endpoint is formed by the base URI from the environment and the version we are about to use,

``https://semple.app/api/v1/user/login``

In order to make a request we need to send the required HTTP headers and the body in JSON formatted.